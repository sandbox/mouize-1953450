<?php

// Receiver types
define('NTF_RECEIVER_ROLE', 'role');
define('NTF_RECEIVER_USER', 'user');

// Read status types
define('NTF_MESSAGE_READ', 1);
define('NTF_MESSAGE_NOT_READ', 0);

// Set default expire lifetime to one day.
define('NTF_EXPIRE_LIFETIME', 86400);

// Set default nb element to display
define('NTF_LIMIT_DISPLAY', 5);

// Set default nb element to display
define('NTF_SUMMARY_TEXT', 150);


/**
 * Implements hook_entity_info().
 */
function simple_notifier_entity_info() {
  return array(
    'simple_notifier' => array(
      'label' => t('simple_notifier'),
      'controller class' => 'simpleNotifierController',
      'base table' => 'simple_notifier',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'id',
      ),
      'module' => 'simple_notifier',
    ),
  );
}

/**
 * Implements hook_entity_property_info_alter().
 */
function simple_notifier_entity_property_info_alter(&$info) {
  $properties = & $info['simple_notifier']['properties'];
  $properties['created'] = array(
    'label' => t("Date created"),
    'type' => 'date',
    'description' => t("The date the node was posted."),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer nodes',
    'schema field' => 'created',
  );

}

/**
 * Implements hook_boot().
 */
function simple_notifier_boot() {
  // Include helpers.
  require_once dirname(__FILE__) . "/includes/simple_notifier.helpers.inc";

  // Include views functions.
  require_once dirname(__FILE__) . "/views/simple_notifier.views.inc";
}


/**
 * Implements hook_menu().
 */
function simple_notifier_menu() {

  $items['admin/config/user-interface/simple_notifier'] = array(
    'title' => 'Simple notifier',
    'description' => 'Configure settings for simple_notifier.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simple_notifier_admin_form'),
    'access arguments' => array('admin_simple_notifier'),
    'file' => 'includes/simple_notifier.admin.inc'
  );

  $items['simple_notifier_ajax'] = array(
    'title' => '',
    'page callback' => 'simple_notifier_ajax_actions',
    'access callback' => TRUE,
  );

  return $items;
}


/**
 * Implements hook_permission().
 */
function simple_notifier_permission() {
  return array(
    'admin_simple_notifier' => array(
      'title' => t('Administer simple_notifier'),
      'description' => t('Let the user to administer notifications'),
    ),
  );
}

/**
 * Implements hook_views_api().
 */
function simple_notifier_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'simple_notifier') . '/views',
  );
}

/**
 * Implements hook_block_info().
 */
function simple_notifier_block_info() {
  // This example comes from node.module.
  $blocks['simple_notifier_list'] = array(
    'info' => t('Notifications'),
    'cache' => DRUPAL_NO_CACHE,
    'visibility' => BLOCK_VISIBILITY_NOTLISTED,
    'pages' => 'admin/simple_notifier',
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function simple_notifier_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'simple_notifier_list':
      $content = _simple_notifier_get_block();
      if ($content) {
        $block['subject'] = t('Notifications');
        $block['content'] = $content;
      }
      break;
  }
  return $block;
}

/**
 * Implements hook_exit().
 */
function simple_notifier_exit() {
  $notification_messages = & drupal_static('simple_notifier_set_notification_message');

  if ($notification_messages) {

    $notification_insert = array();
    foreach ($notification_messages as $notification) {
      $notification_insert[] = array(
        'message_type' => (isset($notification['message_type'])) ? $notification['message_type'] : 'simple_notifier',
        'message' => (isset($notification['message'])) ? $notification['message'] : 'message not formatted correctly',
        'receiver_type' => (isset($notification['receiver_type'])) ? $notification['receiver_type'] : NTF_RECEIVER_USER,
        'receiver_id' => (isset($notification['receiver_id'])) ? $notification['receiver_id'] : 1,
      );
    }

    if ($notification_insert) {
      _simple_notifier_set_notification($notification_insert);
    }
  }
  unset($notification_messages);

}

/**
 * Callback function for ajax call.
 *
 * @param string $js
 *  Defining js or nojs context.
 * @param string $action
 *  Action on the table (all-read, read, delete).
 * @param int $id
 *  Id of the row to effect (0 for the whole table).
 * @param string $class
 *  Class of the td dom element to hide.
 * @param string $table_id
 *  Id of the table dom element.
 *
 * @return string
 *  Json instructions.
 */
function simple_notifier_ajax_actions($js, $action, $id, $class, $table_id) {

  if (!$js) {
    // We don't support degrading this from js because we're not
    // using the server to remember the state of the table.
    return MENU_ACCESS_DENIED;
  }
  ctools_include('ajax');

  switch ($action) {
    case 'all-read':
      _simple_notifier_set_notifications_as_read();
      $ajax_remove = "#block-simple_notifier-simple_notifier-list";
      break;

    case 'read':
    case 'delete':
      if ($action == 'read') {

      }
      _simple_notifier_set_notifications_as_read(array($id));
      $ajax_remove = "tr.$class";
      break;

    case 'delete':
      _simple_notifier_delete_notifications(array($id));
      $ajax_remove = "tr.$class";
      break;
  }

  $commands = array();
  $commands[] = ajax_command_remove($ajax_remove);
  $commands[] = ajax_command_restripe("table.$table_id");
  print ajax_render($commands);
  exit;

}

/**
 * Add a notifications.
 *
 * @param string $message_type
 *  Type of message (user, commerce, module name, ...).
 * @param string $message
 *  Text of the message.
 * @param string receiver_type
 *  Receiver type of the message (user or role).
 * @param int $receiver_id
 *  Receiver id.
 */
function simple_notifier_set_notification_message($message_type, $message, $receiver_type = NTF_RECEIVER_USER, $receiver_id = 1) {
  $notification_messages = & drupal_static(__FUNCTION__);
  $notification_messages[] = array(
    'message_type' => $message_type,
    'message' => $message,
    'receiver_type' => $receiver_type,
    'receiver_id' => $receiver_id,
  );

}