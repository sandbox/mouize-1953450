<?php



function simple_notifier_preprocess_views_view(&$variables) {

  //krumo($variables);
  if ($variables['view']->name != 'simple_notifier') {
    return FALSE;
  }

  $mark_to_read = array();
  foreach($variables['view']->result as $notification) {
    if (!$notification->simple_notifier_is_read) {
      $mark_to_read[] = $notification->id;
    }
  }
  _simple_notifier_set_notifications_as_read($mark_to_read);
}