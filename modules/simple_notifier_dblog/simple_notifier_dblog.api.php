<?php

/**
 * Define all watchdog message_type to catch.
 */
function hook_simple_notifier_dblog_types() {
  return array('php', 'content');
}