<?php
/**
 * @file
 * This file contains the admin functions for simple_notifier dblog module.
 */

/**
 * Admin configuration form for simple_notifier dblog.
 */
function simple_notifier_dblog_admin_form($form, &$form_state) {
  $severity_list = _simple_notifier_dblog_get_severity();
  $severity_list = array('none' => t('None')) + $severity_list;

  foreach ($test as $t) {

  }
  $form['simple_notifier_dblog_severity'] = array(
    '#type' => 'select',
    '#options' => $severity_list,
    '#multiple' => TRUE,
    '#title' => t('Watchdog severity.'),
    '#description' => t('Watchdog severity of messages to catch'),
    '#default_value' => variable_get('simple_notifier_dblog_severity', array('none')),
  );

  $form['simple_notifier_dblog_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Watchdog types.'),
    '#description' => t('Watchdog type to catch. Seperate by a space.'),
    '#default_value' => variable_get('simple_notifier_dblog_type', ''),
  );

  return system_settings_form($form);
}