<?php
/**
 * @file
 * This file contains the admin functions for simple_notifier module.
 */

/**
 * Admin configuration form for simple_simple_notifier.
 */
function simple_notifier_admin_form($form, &$form_state) {

  $form['simple_notifier_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Notification lifetime.'),
    '#description' => t('Define the lifetime of unread notifications'),
    '#default_value' => variable_get('simple_notifier_lifetime', NTF_EXPIRE_LIFETIME),
  );

  $form['simple_notifier_display'] = array(
    '#type' => 'select',
    '#options' => range(0, 10),
    '#title' => t('Number of notifications in the block.'),
    '#description' => t('Define the number of notification to display in the block.'),
    '#default_value' => variable_get('simple_notifier_display', NTF_LIMIT_DISPLAY),
  );

  return system_settings_form($form);
}