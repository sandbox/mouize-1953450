<?php

/********************************************************************
 * ******************** Functions *********************************
 * ******************************************************************
 */

/**
 * Helper: get block content.
 */
function _simple_notifier_get_block() {

  drupal_add_css(drupal_get_path('module', 'simple_notifier') . '/simple_notifier.css');

  ctools_include('ajax');
  ctools_include('modal');

  // Add CTools' javascript to the page.
  ctools_modal_add_js();

  $expired = _simple_notifier_define_expired();
  $conditions = array(
    array('is_read', 0),
    array('expired', $expired, '>'),
  );

  $notifications = _simple_notifier_get_notifications($conditions, array(0, NTF_LIMIT_DISPLAY));

  $rows = $render_array = $links = array();
  $operations_options = array(
    'read' => t('Mark as read'),
    'delete' => t('Delete'),
  );

  $table_id = 'simple_notifier_table';
  foreach ($notifications as $key => $notification) {
    $class = 'simple_notifier_' . $key;

    $actions = theme('item_list', array(
      'items' => array(
        ctools_ajax_text_button(t('Mark as read'), "simple_notifier_ajax/nojs/read/{$notification->id}/$class/$table_id", t('Mark as read')),
        ctools_ajax_text_button(t('Delete'), "simple_notifier_ajax/nojs/delete/{$notification->id}/$class/$table_id", t('Delete')),
      ),
      'title' => ''
    ));

    $summary_link = l(_simple_notifier_summary_text($notification->message), 'admin/simple_notifier');
    $rows[] = array(
      'data' => array($notification->message_type, $summary_link, $actions),
      'class' => array($class),
    );
  }

  if ($rows) {

    $output_links = ctools_ajax_text_button(t('Mark all read'), "simple_notifier_ajax/nojs/all-read/0/all/$table_id", t('Mark as read'));
    $output_links .= ' - ';
    $output_links .= l(t('Read more'), 'admin/simple_notifier');
    $render_array['read_all'] = array(
      '#type' => 'markup',
      '#markup' => $output_links,
    );

    $render_array['notifications'] = array(
      '#theme' => 'table',
      '#header' => array(),
      '#rows' => $rows,
      '#attributes' => array('class' => $table_id),
    );


  }

  return $render_array;
}

/**
 * Helper: get the timestamp where a notification is not expired.
 *
 * @return int
 */
function _simple_notifier_define_expired() {
  $delay = variable_get('simple_notifier_lifetime', NTF_EXPIRE_LIFETIME);
  return time() - $delay;
}


function _simple_notifier_summary_text($str, $n = NTF_SUMMARY_TEXT, $end_char = '&#8230;') {
  if (strlen($str) < $n) {
    return $str;
  }

  $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

  if (strlen($str) <= $n) {
    return $str;
  }

  $out = "";
  foreach (explode(' ', trim($str)) as $val) {
    $out .= $val . ' ';

    if (strlen($out) >= $n) {
      $out = trim($out);
      return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
    }
  }
}

/********************************************************************
 * ******************** Entity api *********************************
 * ******************************************************************
 */


/**
 * Load a notification.
 */
function simple_notifier_load($id, $reset = FALSE) {
  $notifications = simple_notifier_load_multiple(array($id), array(), $reset);
  return reset($notifications);
}

/**
 * Load multiple notifications based on certain conditions.
 */
function simple_notifier_load_multiple($ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('simple_notifier', $ids, $conditions, $reset);
}

/**
 * Save notification.
 */
function simple_notifier_save($notification) {
  entity_save('simple_notifier', $notification);
}

/**
 * Delete single notification.
 */
function simple_notifier_delete($notification) {
  entity_delete('simple_notifier', entity_id('simple_notifier', $notification));
}

/**
 * Delete multiple notifications.
 */
function simple_notifier_delete_multiple($notification_ids) {
  entity_delete_multiple('simple_notifier', $notification_ids);
}

/********************************************************************
 * ******************** DB requests *********************************
 * ******************************************************************
 */

/**
 * Helper: DB request to get all notifications for the current user.
 *
 * @param array $conditions
 *  Contains array of condition as defined by drupal for db_select queries.
 * @param array $limit
 *  Contains two values: offset and nb_elements
 *
 * @return DatabaseStatementInterface|int|null
 */
function _simple_notifier_get_notifications($conditions = array(), $limit = array()) {
  global $user;

  $query = db_select('simple_notifier', 'n')
    ->fields('n');

  $condition1 = db_and()->condition('receiver_type', NTF_RECEIVER_ROLE)
    ->condition('receiver_id', array_keys($user->roles), 'IN');
  $condition2 = db_and()->condition('receiver_type', NTF_RECEIVER_USER)->condition('receiver_id', $user->uid);
  $db_or = db_or()->condition($condition1)->condition($condition2);

  $query->condition($db_or);

  foreach ($conditions as $condition) {
    $query->condition(
      $condition[0],
      $condition[1],
      (isset($condition[2])) ? $condition[2] : '='
    );
  }

  if ($limit) {
    $query->range($limit[0], $limit[1]);
  }
  $query->orderBy('created', 'DESC');

  return $query->execute()
    ->fetchAll();
}

/**
 * Helper: Insert notifications.
 *
 * @param array $values
 *  Values to insert
 *
 * @return DatabaseStatementInterface|int|null
 */
function _simple_notifier_set_notification($values) {

  $query = db_insert('simple_notifier')
    ->fields(array('message_type', 'message', 'receiver_type', 'receiver_id', 'expired', 'created'));

  foreach ($values as $record) {
    $record = array(
      'expired' => time() + variable_get('simple_notifier_lifetime', NTF_EXPIRE_LIFETIME),
      'created' => REQUEST_TIME,
    ) + $record;

    $query->values($record);
  }
  return $query->execute();
}


/**
 * Helper : Update notifications as read.
 * @param array $ids
 */
function _simple_notifier_set_notifications_as_read($ids = array()) {

  $query = db_update('simple_notifier')->fields(array('is_read' => NTF_MESSAGE_READ));
  if ($ids) {
    $query->condition('id', $ids, 'IN');
  }
  $query->execute();
}


/**
 * Helper : Update notifications as read.
 * @param array $ids
 */
function _simple_notifier_delete_notifications($ids) {
  if ($ids) {
    db_delete('simple_notifier')
      ->condition('id', $ids, 'IN')
      ->execute();
  }
}


