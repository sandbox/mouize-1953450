<?php


class simpleNotifierController extends EntityAPIController {

  public function create(array $values = array()) {

    $values += array(
      'message_type' => '',
      'message' => '',
      'receiver_type' => '',
      'receiver_id' => 0,
      'is_read' => 0,
      'expired' => time() + variable_get('simple_notifier_lifetime', NTF_EXPIRE_LIFETIME),
      'created' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  /**
   * Overrides EntityAPIController::buildQuery().
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    global $user;

    $query = parent::buildQuery($ids, $conditions, $revision_id);

    $condition1 = db_and()->condition('receiver_type', NTF_RECEIVER_ROLE)
      ->condition('receiver_id', array_keys($user->roles), 'IN');
    $condition2 = db_and()->condition('receiver_type', NTF_RECEIVER_USER)->condition('receiver_id', $user->uid);
    $db_or = db_or()->condition($condition1)->condition($condition2);
    $query->condition($db_or);

    $query->orderBy('created', 'DESC');

    return $query;
  }

}